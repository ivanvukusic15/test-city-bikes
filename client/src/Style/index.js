import { Platform, StyleSheet } from 'react-native';
import { iPhoneX } from '../Utils';

/**
 * COLOR VARIABLES
 */
export const ultraLightBlue = '#ABBCE0';
export const lightBlue = '#7593CD';
export const blue = '#0A58FD';
export const darkBlue = '#0A339F';
export const darkerBlue = '#0C1E6D';
export const red = '#E82B65';
export const lightGreen = '#19FFA3';
export const green = '#00CC66';
export const darkGreen = '#2A6246';
export const yellow = '#FF9500';
export const ultralightGray = '#EAEAEA';
export const lighterTextColor = '#868686';
export const normalTextColor = '#535353';
export const darkTextColor = '#021E13';
export const darkerTextColor = '#191919';
export const themeYellow = '#FFDA00';
export const themeRed = '#D30000';
export const accent1Color = '#1A1A1A';
export const navBarTextColor = '#FFFFFF';
export const whiteTextColor = '#FFFFFF';
export const disabled = '#E1E6ED';
export const lightBlack = '#303030';
export const darkGray = '#606060';
export const gray = '#838383';
export const lightGray = '#A3A3A3';
export const separatorColor = '#CCCCCC';
export const disabledRow = 'rgb(246, 246, 246)';
export const headerRegistrationColor = '#63B3C7';
export const headerColor = lightBlue;

/**
 * ICONS SIZE VARIABLES
 */
export const smallerIcon = 22;
export const normalIcon = 25;
export const largeIcon = 28;
export const extraLargeIcon = 36;
export const inputHeight = 70;

/**
 * FONT SIZE VARIABLES
 */
export const h1 = 26;
export const h2 = 24;
export const h3 = 22;
export const h4 = 20;
export const h5 = 18;
export const smallerTextSize = 10;
export const smallTextSize = 12;
export const normalTextSize = 14;
export const largerTextSize = 16;

export let headerHeight = 62;
export let topDiff = 34;
export let bottomDiff = 0;
export let notificationPaddingTop = 25;
export let notificationPaddingBottom = 0;
export let cameraMarginBottom = 0;

if (iPhoneX()) {
  headerHeight = 88;
  bottomDiff = 34;
  notificationPaddingTop = 44;
  notificationPaddingBottom = 14;
  cameraMarginBottom = 25;
} else if (Platform.OS === 'android' && Platform.Version < 23) {
  headerHeight = 55;
  topDiff = 20;
} else if (Platform.OS === 'android') {
  headerHeight = 70;
}

export const SEARCHCONTAINERHEIGHT = 60;
export const SEARCHBARHEIGHT = 36;

/**
 * GLOBAL STYLES
 */
export const global = StyleSheet.create({
  // container syle
  container: {
    flex: 1,
  },
  column: {
    flexDirection: 'column',
  },
  row: {
    flexDirection: 'row',
  },
  alignStart: {
    alignItems: 'flex-start',
  },
  alignCenter: {
    alignItems: 'center',
  },
  alignEnd: {
    alignItems: 'flex-end',
  },
  justifyCenter: {
    justifyContent: 'center',
  },
  justifyBetween: {
    justifyContent: 'space-between',
  },
  justifyAround: {
    justifyContent: 'space-around',
  },
  justifyStart: {
    justifyContent: 'flex-start',
  },
  justifyEnd: {
    justifyContent: 'flex-end',
  },
  absoluteFull: {
    ...StyleSheet.absoluteFillObject,
  },
  width100: {
    width: '100%',
  },
  height100: {
    height: '100%',
  },
  contentContainer: {
    padding: 15,
  },
  paddingHorizontal: {
    paddingHorizontal: 15,
  },
  paddingVertical: {
    paddingVertical: 15,
  },
  marginHorizontal: {
    marginHorizontal: 15,
  },
  marginVertical: {
    marginVertical: 15,
  },
  ultralightGrayBackground: {
    backgroundColor: ultralightGray,
  },
  positionAbsolute: {
    position: 'absolute',
  },
  positionRelative: {
    position: 'relative',
  },

  // font color style
  whiteTextColor: {
    color: 'white',
  },
  ultraLightBlueTextColor: {
    color: ultraLightBlue,
  },
  lightBlueTextColor: {
    color: lightBlue,
  },
  blueTextColor: {
    color: blue,
  },
  greenTextColor: {
    color: green,
  },
  darkGreenTextColor: {
    color: darkGreen,
  },
  redTextColor: {
    color: red,
  },
  darkerBlueTextColor: {
    color: darkerBlue,
  },
  lightGrayTextColor: {
    color: lightGray,
  },
  lighterTextColor: {
    color: lighterTextColor,
  },
  normalTextColor: {
    color: normalTextColor,
  },
  darkBlueTextColor: {
    color: darkBlue,
  },
  darkTextColor: {
    color: darkTextColor,
  },
  darkerTextColor: {
    color: darkerTextColor,
  },
  themeYellowTextColor: {
    color: themeYellow,
  },
  themeRedTextColor: {
    color: themeRed,
  },

  // font size
  h1: {
    fontSize: h1,
    lineHeight: h1 + 6,
  },
  h2: {
    fontSize: h2,
    lineHeight: h2 + 6,
  },
  h3: {
    fontSize: h3,
    lineHeight: h3 + 6,
  },
  h4: {
    fontSize: h4,
    lineHeight: h4 + 6,
  },
  h5: {
    fontSize: h5,
    lineHeight: h5 + 6,
  },
  smallerTextSize: {
    fontSize: smallerTextSize,
    lineHeight: smallerTextSize + 4,
  },
  smallTextSize: {
    fontSize: smallTextSize,
    lineHeight: smallTextSize + 4,
  },
  normalTextSize: {
    fontSize: normalTextSize,
    lineHeight: normalTextSize + 4,
  },
  largerTextSize: {
    fontSize: largerTextSize,
    lineHeight: largerTextSize + 4,
  },

  lineThrough: {
    textDecorationLine: 'line-through',
  },
  underline: {
    textDecorationLine: 'underline',
  },

  boldWeight: {
    fontWeight: '600',
  },
  normalWeight: {
    fontWeight: 'normal',
  },

  // text align
  left: {
    textAlign: 'left',
  },
  center: {
    textAlign: 'center',
  },
  right: {
    textAlign: 'right',
  },

  // opacity
  opacity25: {
    opacity: 0.25,
  },
  opacity50: {
    opacity: 0.5,
  },
  opacity75: {
    opacity: 0.75,
  },
  opacity90: {
    opacity: 0.9,
  },
  opacity100: {
    opacity: 1,
  },

  modalBackground: {
    backgroundColor: 'rgba(0,0,0,0.5)',
  },

  // NAVIGATION STYLES
  headerStyle: {
    backgroundColor: headerColor,
    borderWidth: 0,
    borderBottomWidth: 0,
    elevation: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
  },
  headerHidden: {
    opacity: 0,
    backgroundColor: 'transparent',
  },
  tabIcon: {
    width: 20,
    height: 20,
    opacity: 0.6,
  },
  tabIconFocused: {
    opacity: 1,
  },

  // BOTTOM BUTTON
  customBottomButtonModal: {
    paddingTop: 5,
    paddingBottom: notificationPaddingBottom + 5,
  },
  customBottomButton: {
    paddingVertical: 5,
  },
  buttonDisabled: {
    backgroundColor: gray,
  },
});
