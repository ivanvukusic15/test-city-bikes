export const LOCAL_URL = 'http://192.168.1.100:3000/';

export const BASE_URL_AUTH = `${LOCAL_URL}auth/`;
export const BASE_URL = 'http://api.citybik.es/';

export const INITIAL_DATA = 'initial_data';
export const NAVIGATION_UPDATE = 'navigation_update';
export const TYPE_NAV_RESET_STACK = 'nav_reset_stack';

export const TYPE_USER_UPDATE = 'user_update';
export const TYPE_USER_LOGIN = 'user_login';
export const TYPE_USER_LOGOUT = 'user_logout';

export const TYPE_NETWORKS_GET = 'networks_get';
export const TYPE_NETWORK_GET = 'network_get';
export const TYPE_NETWORK_RESET = 'network_reset';
export const TYPE_NETWORKS_UPDATE = 'networks_update';
export const TYPE_NETWORK_UPDATE_FAVOURITES = 'network_update_favourites';

export const TYPE_HELPER_UPDATE = 'helper_update';
