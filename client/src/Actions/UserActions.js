import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationActions, StackActions } from 'react-navigation';
import { BASE_URL_AUTH, TYPE_USER_LOGIN, TYPE_USER_LOGOUT } from '../Constants';

export const register = ({ email, password, confirmPassword, cb, errorCb }) => {
  return dispatch => {
    axios
      .post(`${BASE_URL_AUTH}register`, { email, password, confirmPassword })
      .then(async response => {
        const { data, error } = response.data;
        if (error && errorCb) {
          if (error.message) {
            errorCb(error.message);
          } else {
            errorCb('Something went wrong');
          }
        } else if (data && data.accessToken) {
          await AsyncStorage.setItem('testCity_accessToken', data.accessToken);
          if (cb) {
            cb();
          }
          dispatch({
            type: TYPE_USER_LOGIN,
            payload: data,
          });
        } else {
          errorCb('Something went wrong');
        }
      })
      .catch(() => {
        errorCb('Something went wrong');
      });
  };
};

export const login = ({ email, password, cb, errorCb }) => {
  return async dispatch => {
    axios
      .post(`${BASE_URL_AUTH}login`, { email, password })
      .then(async response => {
        const { data, error } = response.data;
        if (error && errorCb) {
          if (error.message) {
            errorCb(error.message);
          } else {
            errorCb('Something went wrong');
          }
        } else if (data && data.accessToken) {
          await AsyncStorage.setItem('testCity_accessToken', data.accessToken);
          if (cb) {
            cb();
          }
          dispatch({
            type: TYPE_USER_LOGIN,
            payload: data,
          });
        } else {
          errorCb('Something went wrong');
        }
      })
      .catch(() => {
        errorCb('Something went wrong');
      });
  };
};

export const emailVerify = ({ email, cb }) => {
  axios
    .post(`${BASE_URL_AUTH}emailVerify`, { email })
    .then(async response => {
      console.log(response);
      const { data } = response.data;
      if (data.code === 'exists') {
        cb(data.code);
      } else {
        cb();
      }
    })
    .catch(error => {
      console.log(error);
    });
};

export const logout = () => {
  return async dispatch => {
    await AsyncStorage.multiRemove(['testCity_accessToken', 'testCity_favorites']);
    dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Registration' })],
      }),
    );
    dispatch({
      type: TYPE_USER_LOGOUT,
    });
  };
};
