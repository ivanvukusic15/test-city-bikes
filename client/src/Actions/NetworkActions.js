import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {
  BASE_URL,
  TYPE_NETWORKS_GET,
  TYPE_NETWORK_GET,
  TYPE_NETWORK_RESET,
  TYPE_NETWORKS_UPDATE,
  TYPE_NETWORK_UPDATE_FAVOURITES,
} from '../Constants';

export const getNetworks = ({ errorCb }) => {
  return async dispatch => {
    dispatch({
      type: TYPE_NETWORKS_UPDATE,
      payload: { prop: 'loading', value: true },
    });
    axios
      .get(`${BASE_URL}/v2/networks`)
      .then(({ data }) => {
        if (data && data.networks) {
          const countries = generateRandomColors(data.networks);
          dispatch({
            type: TYPE_NETWORKS_GET,
            payload: { networks: data.networks, countries },
          });
        }
      })
      .catch(() => {
        dispatch({
          type: TYPE_NETWORKS_UPDATE,
          payload: { prop: 'loading', value: false },
        });
        if (errorCb) {
          errorCb();
        }
      });
  };
};

export const getNetwork = ({ url, errorCb }) => {
  return async dispatch => {
    dispatch({
      type: TYPE_NETWORKS_UPDATE,
      payload: { prop: 'loading', value: true },
    });
    console.log(`${BASE_URL}${url}`);
    axios
      .get(`${BASE_URL}${url}`)
      .then(({ data }) => {
        console.log(data);
        if (data && data.network) {
          dispatch({
            type: TYPE_NETWORK_GET,
            payload: data.network,
          });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: TYPE_NETWORKS_UPDATE,
          payload: { prop: 'loading', value: false },
        });
        if (errorCb) {
          errorCb();
        }
      });
  };
};

export const resetNetwork = () => {
  return {
    type: TYPE_NETWORK_RESET,
  };
};

export const updateFavouriteStations = (favourites, station) => {
  return dispatch => {
    if (favourites[station.id]) {
      delete favourites[station.id];
    } else {
      favourites[station.id] = station;
    }
    AsyncStorage.setItem('testCity_favorites', JSON.stringify(favourites));
    dispatch({
      type: TYPE_NETWORK_UPDATE_FAVOURITES,
      payload: { favourites },
    });
  };
};

const generateRandomColors = data => {
  const countries = {};
  data.map(network => {
    if (network.location) {
      countries[network.location.country] = `rgba(${Math.round(Math.random() * 255)},${Math.round(
        Math.random() * 255,
      )}, ${Math.round(Math.random() * 255)}, 0.4)`;
    }
  });
  return countries;
};
