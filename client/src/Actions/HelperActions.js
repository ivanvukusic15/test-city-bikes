import AsyncStorage from '@react-native-community/async-storage';
import { AppNavigator } from '../Navigators/MainNavigator';
import { INITIAL_DATA } from '../Constants';

export const getInitialData = cb => {
  return async dispatch => {
    const storage = await AsyncStorage.multiGet(['testCity_accessToken', 'testCity_favorites']);
    let favourites = null;
    let accessToken = null;
    let screen = 'Registration';
    if (storage) {
      storage.map(item => {
        switch (item[0]) {
          case 'testCity_accessToken':
            if (item[1]) {
              // eslint-disable-next-line
              accessToken = item[1];
              screen = 'Dashboard';
            }
            break;
          case 'testCity_favorites':
            try {
              favourites = JSON.parse(item[1]);
            } catch (error) {
              console.log(error);
            }
            break;
          default:
            break;
        }
      });
      const nav = AppNavigator.router.getStateForAction(AppNavigator.router.getActionForPathAndParams(screen));
      dispatch({
        type: INITIAL_DATA,
        payload: { accessToken, favourites, nav },
      });
      cb(screen);
    }
  };
};
