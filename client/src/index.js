/**
 * @flow
 */
import React, { Component } from 'react';
import { View, BackHandler } from 'react-native';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import { NavigationActions } from 'react-navigation';
import { Alert, DoneBar, Error } from './Components';
import type { AlertType, DoneBarType, ErrorType } from './Types';
import { AppWithNavigationState, store } from './AppWithNavigationState';
import { getInitialData } from './Actions';

export default class App extends Component<Props, State> {
  /* eslint-disable */
  alert: any;
  doneBar: any;
  backHandlerListener: any;
  error: any;
  /* eslint-enable */

  state = {
    screen: '',
  };

  componentDidMount() {
    this.backHandlerListener = BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    store.dispatch(getInitialData(screen => this.setState({ screen })));
  }

  componentWillUnmount() {
    this.backHandlerListener.remove();
  }

  onBackPress = () => {
    const { nav } = store.getState();
    if (nav.index === 0) {
      return false;
    }
    store.dispatch(NavigationActions.back());
    return true;
  };

  getChildContext() {
    return {
      setAlert: this.setAlert,
      setGlobalError: this.setGlobalError,
      setKeyboardBarButtons: this.setKeyboardBarButtons,
      removeKeyboardBarButtons: this.removeKeyboardBarButtons,
    };
  }

  setGlobalError = (error: ErrorType) => this.error.setState({ error });

  setAlert = (alert: AlertType) => this.alert.setState({ alert });

  removeKeyboardBarButtons = () => this.doneBar.hide();

  setKeyboardBarButtons = ({ current, inputRefs }: DoneBarType) => this.doneBar.show({ current, inputRefs });

  render() {
    const { screen } = this.state;
    if (screen) {
      return (
        <Provider store={store}>
          <View style={{ flex: 1 }}>
            <AppWithNavigationState />
            <DoneBar
              ref={ref => {
                this.doneBar = ref;
              }}
            />
            <Alert
              ref={ref => {
                this.alert = ref;
              }}
            />
            <Error
              ref={ref => {
                this.error = ref;
              }}
            />
          </View>
        </Provider>
      );
    }
    return null;
  }
}

App.childContextTypes = {
  setAlert: PropTypes.func,
  setGlobalError: PropTypes.func,
  setKeyboardBarButtons: PropTypes.func,
  removeKeyboardBarButtons: PropTypes.func,
};

type Props = {};
type State = {
  screen: string,
};
