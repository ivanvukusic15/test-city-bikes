// @flow
import React from 'react';
import { TextInput, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { styles } from './SearchBar.style';
import { global, darkerTextColor, themeRed } from '../../Style/index';

export const SearchBar = ({ onChangeText, onReset, placeholder, value }: Props) => (
  <View style={[global.row, global.alignCenter, global.contentContainer, styles.container]}>
    <Icon style={styles.searchIcon} size={22} color={darkerTextColor} name={'search'} />
    <TextInput
      style={[global.container, styles.input]}
      value={value}
      placeholder={placeholder}
      underlineColorAndroid="transparent"
      onChangeText={onChangeText}
    />
    {!!value && (
      <TouchableOpacity activeOpacity={0.9} onPress={onReset}>
        <Icon size={22} color={themeRed} name={'times'} />
      </TouchableOpacity>
    )}
  </View>
);

type Props = {
  onChangeText: Function,
  onReset: Function,
  placeholder?: string,
  value: string,
};
