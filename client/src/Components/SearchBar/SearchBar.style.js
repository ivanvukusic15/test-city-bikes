import { StyleSheet } from 'react-native';
import { ultralightGray } from '../../Style/index';

export const styles = StyleSheet.create({
  container: {
    paddingVertical: 6,
    borderWidth: 1,
    borderColor: ultralightGray,
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 2,
  },
  searchIcon: {
    marginBottom: 4,
  },
  input: {
    paddingHorizontal: 6,
    height: 45,
  },
});
