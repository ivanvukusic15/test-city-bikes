import { StyleSheet } from 'react-native';
import { lightBlue } from '../../Style';

export const styles = StyleSheet.create({
  container: {
    marginBottom: 20,
  },
  label: {
    marginBottom: 10,
  },
  error: {
    marginLeft: 20,
  },
  inputContainer: {
    borderWidth: 1,
    borderColor: '#DADADA',
    padding: 8,
    borderRadius: 4,
    paddingRight: 50,
  },
  inputContainerFocused: {
    borderColor: lightBlue,
  },
  input: {
    paddingVertical: 8,
  },
  iconContainer: {
    position: 'absolute',
    right: 0,
    top: 0,
    bottom: 0,
  },
  iconStyle: {
    marginRight: 8,
  },
  spinnerStyle: {
    marginRight: 15,
  },
});
