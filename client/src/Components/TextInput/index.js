import React, { Component } from 'react';
import { TextInput, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { Text, Spinner } from '..';
import { styles } from './TextInput.style';
import { global, lightBlue, darkerTextColor } from '../../Style';

export class CustomTextInput extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      focused: false,
      secureTextEntry: props.secureTextEntry,
    };
  }

  onBlur = () => {
    this.setState({ focused: false });
    this.props.onBlur();
  };

  onFocus = () => {
    this.setState({ focused: true });
    this.props.onFocus();
  };

  onEyePress = () => this.setState(state => ({ secureTextEntry: !state.secureTextEntry }));

  render() {
    const {
      autoFocus,
      error,
      keyboardType,
      iconColor,
      iconName,
      iconSize,
      iconStyle,
      label,
      loading,
      onChangeText,
      placeholder,
      value,
    } = this.props;
    const { focused, secureTextEntry } = this.state;
    return (
      <View style={styles.container}>
        <View style={[global.row, global.justifyBetween, global.alignCenter, styles.label]}>
          <Text>{label}</Text>
          {loading && (
            <Text style={[global.smallerTextSize, global.lightBlueTextColor]}>checking email availability...</Text>
          )}
          {!loading && !!error && (
            <Text
              style={[global.container, global.right, global.smallerTextSize, global.redTextColor, styles.error]}
              numberOfLines={1}>
              {error}
            </Text>
          )}
        </View>
        <View style={[styles.inputContainer, focused && styles.inputContainerFocused]}>
          <TextInput
            style={styles.input}
            autoCapitalize={'none'}
            autoFocus={autoFocus}
            keyboardType={keyboardType}
            value={value}
            secureTextEntry={secureTextEntry}
            placeholder={placeholder}
            underlineColorAndroid="transparent"
            onChangeText={onChangeText}
            onBlur={this.onBlur}
            onFocus={this.onFocus}
          />
          <View style={[styles.iconContainer, global.row, global.alignCenter, global.justifyCenter]}>
            {loading && <Spinner customStyle={styles.spinnerStyle} color={lightBlue} />}
            {!loading && !!iconName && (
              <Icon style={[styles.iconStyle, iconStyle]} name={iconName} size={iconSize} color={iconColor} />
            )}
            {this.props.secureTextEntry && (
              <TouchableOpacity activeOpacity={0.9} onPress={this.onEyePress}>
                <Icon
                  style={[styles.iconStyle, iconStyle]}
                  name={secureTextEntry ? 'eye' : 'eye-slash'}
                  size={20}
                  color={darkerTextColor}
                />
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    );
  }
}

CustomTextInput.defaultProps = {
  autoFocus: false,
  error: '',
  iconName: '',
  iconSize: 0,
  iconColor: '',
  iconStyle: {},
  loading: false,
  onBlur: () => {},
  onChangeText: () => {},
  onFocus: () => {},
  returnKeyType: 'default',
  placeholder: '',
  secureTextEntry: false,
};

type Props = {
  autoFocus?: boolean,
  error?: string,
  iconName?: any,
  iconSize?: number,
  iconColor?: string,
  iconStyle?: Object | Array<Object>,
  label: string,
  loading?: boolean,
  onBlur?: Function,
  onChangeText?: Function,
  onFocus?: Function,
  placeholder?: string,
  returnKeyType?: string,
  keyboardType: string,
  secureTextEntry: boolean,
  value: string,
};

type State = {
  focused: boolean,
};
