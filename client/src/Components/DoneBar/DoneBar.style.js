import { StyleSheet } from 'react-native';

export const DONE_BAR_HEIGHT = 42;

export const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'white',
  },
  innerContainer: {
    height: DONE_BAR_HEIGHT,
    backgroundColor: 'white',
    paddingHorizontal: 10,
    elevation: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: -0.3 },
    shadowOpacity: 0.2,
    shadowRadius: 1,
  },
  shadow: {
    elevation: 1,
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 0.8,
  },
  arrow: {
    padding: 2,
    paddingHorizontal: 6,
    marginRight: 5,
  },
});
