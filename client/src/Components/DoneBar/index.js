// @flow
import React, { PureComponent } from 'react';
import { Platform, View, KeyboardAvoidingView, Keyboard, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { Text } from '..';
import { styles, DONE_BAR_HEIGHT } from './DoneBar.style';
import { global, normalTextColor } from '../../Style';

export class DoneBar extends PureComponent<Props, State> {
  /* eslint-disable */
  keyboardDidShowListener: any;
  keyboardDidHideListener: any;
  inputRefs: Object;
  /* eslint-enable */

  constructor(props: Props) {
    super(props);
    this.inputRefs = {};
    this.state = {
      arrowsVisible: false,
      current: '',
      keyboardHeight: 0,
      visible: false,
    };
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
      this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
    } else {
      this.keyboardDidHideListener = Keyboard.addListener('keyboardWillHide', this.keyboardDidHide);
    }
  }

  componentWillUnmount() {
    if (this.keyboardDidShowListener) {
      this.keyboardDidShowListener.remove();
    }
    if (this.keyboardDidHideListener) {
      this.keyboardDidHideListener.remove();
    }
  }

  keyboardDidShow = (event: Object) => {
    if (event.endCoordinates.height !== this.state.keyboardHeight) {
      this.setState({ keyboardHeight: event.endCoordinates.height });
    }
  };

  keyboardDidHide = () => this.hide();

  onArrowDown = () => {
    let current = null;
    Object.keys(this.inputRefs).map((key, i) => {
      if (key === this.state.current) {
        current = i;
      }
    });
    let next = current + 1;
    if (next >= Object.keys(this.inputRefs).length) {
      next = 0;
    }
    this.goToNext(Object.keys(this.inputRefs)[next]);
  };

  onArrowUp = () => {
    let current = 0;
    Object.keys(this.inputRefs).map((key, i) => {
      if (key === this.state.current) {
        current = i;
      }
    });
    let next = current - 1;
    if (next < 0) {
      next = Object.keys(this.inputRefs).length - 1;
    }
    this.goToNext(Object.keys(this.inputRefs)[next]);
  };

  goToNext(next: string) {
    let ref = null;
    if (this.inputRefs[next]) {
      if (this.inputRefs[next].textInput) {
        if (this.inputRefs[next].textInput.focus) {
          ref = this.inputRefs[next].textInput;
        } else if (this.inputRefs[next].textInput.input && this.inputRefs[next].textInput.input.focus) {
          ref = this.inputRefs[next].textInput.input;
        }
      } else if (this.inputRefs[next].input) {
        if (this.inputRefs[next].input.focus) {
          ref = this.inputRefs[next].input;
        }
      }
    }
    this.setState({ current: next }, () => {
      if (ref) {
        ref.focus();
      }
    });
  }

  show({ current, inputRefs }: { current: string, inputRefs: Object }) {
    if (!this.state.visible) {
      this.inputRefs = inputRefs;
      this.setState({
        arrowsVisible: inputRefs && !!Object.keys(inputRefs).length,
        current,
        visible: true,
      });
    }
  }

  hide() {
    this.inputRefs = {};
    this.setState({ arrowsVisible: false, current: '', visible: false });
    Keyboard.dismiss();
  }

  onDonePress = () => this.hide();

  render() {
    const { arrowsVisible, keyboardHeight, visible } = this.state;
    const Container = Platform.OS === 'ios' ? KeyboardAvoidingView : View;
    return (
      <Container
        style={[
          styles.container,
          {
            opacity: visible ? 1 : 0,
            height: visible ? DONE_BAR_HEIGHT : 0,
            bottom: visible ? keyboardHeight : 0,
          },
        ]}
        behavior="position"
        enabled>
        <View style={[global.row, global.alignCenter, global.justifyBetween, styles.innerContainer]}>
          {arrowsVisible ? (
            <View style={[global.row]}>
              <TouchableOpacity activeOpacity={0.9} style={styles.arrow} onPress={this.onArrowUp}>
                <Icon style={styles.shadow} name={'angle-up'} color={normalTextColor} size={32} />
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.9} style={styles.arrow} onPress={this.onArrowDown}>
                <Icon style={styles.shadow} name={'angle-down'} color={normalTextColor} size={32} />
              </TouchableOpacity>
            </View>
          ) : (
            <View />
          )}
          <TouchableOpacity activeOpacity={0.9} onPress={this.onDonePress} style={{ paddingHorizontal: 8 }}>
            <Text style={global.boldWeight}>DONE</Text>
          </TouchableOpacity>
        </View>
      </Container>
    );
  }
}

type Props = {
  current: string,
};
type State = {
  arrowsVisible: boolean,
  current: string,
  keyboardHeight: number,
  visible: boolean,
};
