// @flow
import React, { Component } from 'react';
import { Error } from './Error';
import type { ErrorType } from '../../Types';

export class ErrorWrapper extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      error: null,
    };
  }

  render() {
    const { error } = this.state;
    return <Error error={error} />;
  }
}

type Props = {};
type State = {
  error: ErrorType,
};

export default ErrorWrapper;
