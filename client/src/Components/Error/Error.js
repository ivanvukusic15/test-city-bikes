// @flow
import React, { Component } from 'react';
import { Animated, View, TouchableOpacity } from 'react-native';
import { Text } from '..';
import { styles } from './Error.style';
import { global, notificationPaddingTop } from '../../Style';
import type { ErrorType } from '../../Types';

const OPEN_CLOSE_DURATION = 600;
const TRANSLATE_OUT = -400;

export class Error extends Component<Props, State> {
  currentNotificationInterval: any;

  static getDerivedStateFromProps(props: Props, state: State) {
    if (props.error && props.error !== state.error) {
      Animated.timing(state.animatedValue, { toValue: 1, duration: OPEN_CLOSE_DURATION }).start();
      setTimeout(
        () => {
          Animated.timing(state.animatedValue, { toValue: 0, duration: OPEN_CLOSE_DURATION }).start();
        },
        props.error.duration ? props.error.duration : 5000,
      );
      return {
        error: props.error,
        message: props.error.message,
        title: props.error.title,
      };
    } else if (state.error && !props.error) {
      Animated.timing(state.animatedValue, { toValue: 0, duration: OPEN_CLOSE_DURATION }).start();
      return { error: null };
    }
    return null;
  }

  constructor(props: Props) {
    super(props);
    this.state = {
      animatedValue: new Animated.Value(0),
      error: null, // eslint-disable-line
      message: '',
      title: '',
    };
  }

  onClose = () => {
    Animated.timing(this.state.animatedValue, { toValue: 0, duration: OPEN_CLOSE_DURATION }).start();
    if (this.props.error && this.props.error.cb) {
      this.props.error.cb();
    }
  };

  render() {
    const { animatedValue, title, message } = this.state;
    return (
      <Animated.View
        style={[
          styles.container,
          {
            transform: [
              {
                translateY: animatedValue.interpolate({
                  inputRange: [0, 1],
                  outputRange: [TRANSLATE_OUT, 0],
                }),
              },
            ],
          },
        ]}>
        <TouchableOpacity style={{ flex: 1 }} activeOpacity={1} onPress={this.onClose}>
          <View style={[styles.errorContainer, notificationPaddingTop > 0 && { paddingTop: notificationPaddingTop }]}>
            <View style={styles.textContainer}>
              <Text style={[global.normalTextSize, global.boldWeight, global.normalTextColor]}>{title}</Text>
              <Text style={[styles.message, global.smallTextSize, global.normalTextColor]}>{message}</Text>
            </View>

            <View style={styles.footer} />
          </View>
        </TouchableOpacity>
      </Animated.View>
    );
  }
}

type Props = {
  error: ErrorType,
};

type State = {
  animatedValue: any,
  error: ErrorType,
  title: string,
  message: string,
};
