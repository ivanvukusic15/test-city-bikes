import { StyleSheet } from 'react-native';
import { lightBlue } from '../../Style';

export const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },

  errorContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    backgroundColor: 'white',
    opacity: 0.96,
    width: '100%',
    elevation: 2,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
  },
  textContainer: {
    marginHorizontal: 15,
  },
  message: {
    marginTop: 4,
  },
  footer: {
    backgroundColor: lightBlue,
    borderRadius: 5,
    alignSelf: 'center',
    height: 5,
    width: 35,
    margin: 8,
    opacity: 0.9,
  },
});
