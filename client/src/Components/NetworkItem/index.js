// @flow
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { getName } from 'country-list';
import { Text } from '..';
import { styles } from './NetworkItem.style';
import { global, lighterTextColor } from '../../Style';

export const NetworkItem = ({ color, city, country, isLast, name, onPress }: Props) => (
  <TouchableOpacity
    activeOpacity={0.9}
    style={[global.row, global.justifyBetween, global.contentContainer, !isLast && styles.container]}
    onPress={onPress}>
    <View style={[styles.avatar, { backgroundColor: color }]}>
      <Text style={global.largerTextSize}>{country}</Text>
    </View>
    <View style={global.container}>
      <Text style={global.boldWeight}>{name}</Text>
      {country && <Text>Country: {getName(country)}</Text>}
      {city && <Text>City: {city}</Text>}
    </View>
    <View>
      <Icon size={42} color={lighterTextColor} name={'chevron-right'} />
    </View>
  </TouchableOpacity>
);

type Props = {
  city: string,
  color: string,
  country: string,
  isLast: boolean,
  name: string,
  onPress: Function,
};
