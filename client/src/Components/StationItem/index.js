// @flow
import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import { Text } from '..';
import { styles } from './StationItem.style';
import { global, lightGray, normalTextColor, themeYellow } from '../../Style';

export class StationItem extends Component<Props, State> {
  static getDerivedStateFromProps(props: Props, state: State) {
    if (!state.item && props.item) {
      return {
        favorite: props.favorite,
        item: props.item,
        isLast: props.isLast,
      };
    } else if (props.isLast !== state.isLast) {
      return {
        isLast: props.isLast,
      };
    }
    return null;
  }

  constructor(props: Props) {
    super(props);
    this.state = {
      isLast: false,
      item: null,
      favorite: false,
    };
  }

  onStarPress = () => {
    this.setState(
      state => ({ favorite: !state.favorite }),
      () => {
        this.props.onStarPress();
      },
    );
  };

  render() {
    const { favorite, isLast, item } = this.state;
    const { onPress } = this.props;
    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={[
          global.row,
          global.justifyBetween,
          global.alignCenter,
          global.contentContainer,
          !isLast && styles.container,
        ]}
        onPress={onPress}>
        <TouchableOpacity activeOpacity={0.9} style={styles.starContainer} onPress={this.onStarPress}>
          <Icon size={30} color={favorite ? themeYellow : normalTextColor} name={favorite ? 'star' : 'star-border'} />
        </TouchableOpacity>
        <View style={global.container}>
          <Text style={global.boldWeight}>{item.name}</Text>
          <Text>Free bikes: {item.free_bikes}</Text>
          <Text>Date: {item.timestamp ? moment(item.timestamp).format('DD.MM.YYYY') : '-'}</Text>
        </View>
        <View>
          <Icon size={32} color={lightGray} name={'chevron-right'} />
        </View>
      </TouchableOpacity>
    );
  }
}

type Props = {
  favorite: boolean,
  item: Object,
  isLast: boolean,
  onPress: Function,
  onStarPress: Function,
};
type State = {
  favorite: boolean,
  item: Object,
  isLast: boolean,
};
