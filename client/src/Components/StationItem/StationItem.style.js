import { StyleSheet } from 'react-native';
import { ultralightGray, ultraLightBlue } from '../../Style/index';

export const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderBottomColor: ultralightGray,
  },
  avatar: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 60,
    height: 60,
    borderRadius: 30,
    marginRight: 10,
    backgroundColor: `${ultraLightBlue}BF`,
  },
  starContainer: {
    padding: 15,
    paddingLeft: 0,
  },
});
