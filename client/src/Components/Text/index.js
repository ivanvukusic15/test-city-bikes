// @flow
import React from 'react';
import { Text } from 'react-native';
import { global } from '../../Style';

export const CustomText = (props: any) => (
  <Text {...props} style={[global.normalTextSize, global.normalTextColor, global.fontRegular, props.style]} />
);
