// @flow
import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { Spinner, Text } from '..';
import { styles } from './Button.style';

export const Button = ({
  activeOpacity,
  customStyle,
  customTextStyle,
  disabled,
  iconColor,
  iconName,
  iconSize,
  iconStyle,
  label,
  loading,
  onPress,
  spinnerColor,
  testID,
}: Props) => (
  <TouchableOpacity
    activeOpacity={activeOpacity}
    disabled={disabled || loading}
    onPress={() => onPress()}
    style={[styles.buttonStyle, customStyle]}
    testID={testID}>
    {loading && (
      <View style={{ width: 30 }}>
        <Spinner customStyle={{ maxWidth: 30 }} size={'small'} color={spinnerColor} />
      </View>
    )}
    {!loading && iconName && (
      <Icon style={[styles.iconStyle, iconStyle]} name={iconName} size={iconSize} color={iconColor} />
    )}
    <Text style={[styles.textStyle, customTextStyle]}>{label}</Text>
    {(loading || iconName) && <View style={{ width: 30 }} />}
  </TouchableOpacity>
);

Button.defaultProps = {
  activeOpacity: 0.8,
  customStyle: null,
  customTextStyle: null,
  disabled: false,
  loading: false,
  iconName: null,
  iconSize: 0,
  iconColor: '',
  iconStyle: null,
  spinnerColor: 'white',
  testID: '',
};

type Props = {
  activeOpacity?: number,
  customStyle?: Object | Array<Object>,
  customTextStyle?: Object | Array<Object>,
  disabled?: boolean,
  label: string,
  loading?: boolean,
  iconName?: any,
  iconSize?: number,
  iconColor?: string,
  iconStyle?: Object | Array<Object>,
  onPress: Function,
  spinnerColor?: string,
  testID?: string,
};
