import { StyleSheet } from 'react-native';
import { lightBlue } from '../../Style';

export const styles = StyleSheet.create({
  buttonStyle: {
    width: '100%',
    backgroundColor: lightBlue,
    minHeight: 45,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
  },
  textStyle: {
    alignSelf: 'center',
    padding: 10,
    color: 'white',
  },
  iconStyle: {
    marginRight: 5,
  },
});
