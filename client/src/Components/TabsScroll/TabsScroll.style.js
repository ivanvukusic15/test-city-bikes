import { StyleSheet } from 'react-native';
import { headerRegistrationColor } from '../../Style';

export const styles = StyleSheet.create({
  indicatorHeight: {
    height: 4,
    backgroundColor: headerRegistrationColor,
  },
  indicatorContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  tabButtonsContainer: {
    height: 44,
    position: 'relative',
    shadowOffset: { width: 2, height: 0 },
    shadowColor: 'gray',
    shadowOpacity: 0.5,
    elevation: 2,
    backgroundColor: 'white',
    zIndex: 1000,
  },
});
