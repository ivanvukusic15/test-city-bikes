// @flow
import React, { Component } from 'react';
import { Animated, Dimensions, View, TouchableOpacity } from 'react-native';
import { global } from '../../Style';
import { CustomText } from '../Text';
import { styles } from './TabsScroll.style';

const { width } = Dimensions.get('window');

export class TabsScroll extends Component<Props, State> {
  scrollViewContainer: any;

  constructor(props: Props) {
    super(props);
    this.state = {
      translationX: new Animated.Value(0),
    };
  }

  _onTabPress = (index: number) => () => {
    if (this.props.onTabPress) {
      this.props.onTabPress(this.props.tabs[index]);
    }
    Animated.spring(this.state.translationX, { toValue: width * -index, useNativeDriver: true }).start();
    this.scrollViewContainer.scrollTo({ x: width * index, y: 0 });
  };

  render() {
    const { tabs } = this.props;
    return (
      <View style={[global.container]}>
        <View style={[global.row, styles.tabButtonsContainer]}>
          {tabs.map((tab, i) => (
            <TouchableOpacity
              style={[global.container, global.alignCenter, global.justifyCenter]}
              key={tab}
              activeOpacity={0.9}
              onPress={this._onTabPress(i)}>
              <CustomText>{tab}</CustomText>
            </TouchableOpacity>
          ))}
          <Animated.View
            style={[
              styles.indicatorContainer,
              styles.indicatorHeight,
              {
                width: width / tabs.length,
                transform: [{ translateX: Animated.divide(this.state.translationX, tabs.length) }],
              },
            ]}>
            <View style={styles.indicatorHeight} />
          </Animated.View>
        </View>

        <Animated.ScrollView
          ref={ref => {
            this.scrollViewContainer = ref && ref._component;
          }}
          style={global.container}
          pagingEnabled
          horizontal
          scrollEventThrottle={16}
          showsHorizontalScrollIndicator={false}
          onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: this.state.translationX } } }], {
            useNativeDriver: true,
          })}>
          {this.props.children}
        </Animated.ScrollView>
      </View>
    );
  }
}

type Props = {
  children: any,
  onTabPress?: Function,
  tabs: Array<string>,
};
type State = {
  translationX: any,
};
