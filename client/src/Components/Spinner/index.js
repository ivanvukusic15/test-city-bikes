// @flow
import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { styles } from './Spinner.style';
import { darkerTextColor } from '../../Style';

export const Spinner = ({ color, customStyle, size }: Props) => (
  <View style={[styles.spinnerStyle, customStyle]}>
    <ActivityIndicator color={color} size={size} />
  </View>
);

Spinner.defaultProps = {
  customStyle: null,
  size: 'small',
  color: darkerTextColor,
};

type Props = {
  color?: string,
  customStyle?: Object | Array<Object>,
  size?: 'small' | 'large',
};
