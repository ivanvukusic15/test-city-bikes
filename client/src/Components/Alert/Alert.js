// @flow
import React, { Component } from 'react';
import { Animated, View, TouchableOpacity } from 'react-native';
import { Button, Text } from '..';
import { styles } from './Alert.style';
import { global } from '../../Style';
import type { AlertType } from '../../Types';

const OPEN_CLOSE_DURATION = 250;

export class Alert extends Component<Props, State> {
  static getDerivedStateFromProps(props: Props, state: State) {
    if (props.alert && props.alert !== state.alert) {
      Animated.timing(state.animatedValue, { toValue: 1, duration: OPEN_CLOSE_DURATION }).start();
      return {
        alert: props.alert,
        buttons: props.alert.buttons,
        message: props.alert.message,
        title: props.alert.title,
      };
    } else if (state.alert && !props.alert) {
      Animated.timing(state.animatedValue, { toValue: 0, duration: OPEN_CLOSE_DURATION }).start();
      return { alert: null };
    }
    return null;
  }

  constructor(props: Props) {
    super(props);
    this.state = {
      alert: null, // eslint-disable-line
      animatedValue: new Animated.Value(0),
      buttons: null,
      message: null,
      title: null,
    };
  }

  close = () => this.props.close();

  onButtonPress(action: Function) {
    action();
    this.props.close();
  }

  renderButtons() {
    const { buttons } = this.state;
    if (buttons && buttons.length) {
      const renderButtons = [];
      buttons.map((button, i) => {
        const buttonStyle = [styles.customButtonStyle];
        if (i === 0) {
          buttonStyle.push(styles.buttonFirst);
        }
        renderButtons.push(
          <Button
            key={`${button.label}-button-${i + 1}`}
            customContainerStyle={buttonStyle}
            customStyle={{ minHeight: 40 }}
            customTextStyle={[global.normalTextSize, global.fontBold, button.textStyle]}
            label={button.label}
            onPress={() => this.onButtonPress(button.action)}
          />,
        );
      });
      renderButtons.push(
        <Button
          key={`Cancel-button-${buttons.length + 1}`}
          customContainerStyle={[styles.customButtonStyle, styles.buttonLast]}
          customStyle={{ minHeight: 40 }}
          customTextStyle={[global.normalTextSize, global.darkTextColor]}
          label={'Cancel'}
          onPress={this.close}
        />,
      );
      return renderButtons;
    }
    return null;
  }

  render() {
    const { animatedValue, message, title } = this.state;
    return (
      <Animated.View
        style={[
          styles.container,
          {
            opacity: animatedValue.interpolate({
              inputRange: [0, 1],
              outputRange: [0, 1],
            }),
          },
        ]}>
        <TouchableOpacity style={styles.container} onPress={this.close} />
        <View style={styles.innerContainer}>
          <View style={styles.textContainer}>
            <Text style={[global.largerTextSize, global.fontBold, global.center]}>{title}</Text>
            <View style={styles.fullLine} />
            <Text style={[global.center, styles.message]}>{message}</Text>
          </View>
          <View style={[global.container, global.row]}>{this.renderButtons()}</View>
        </View>
      </Animated.View>
    );
  }
}

type Props = {
  alert: AlertType,
  close: Function,
};
type State = AlertType & {
  alert: AlertType,
  animatedValue: any,
};

export default Alert;
