import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0,0,0,0.4)',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 100,
  },
  innerContainer: {
    width: '90%',
    maxWidth: 500,
    backgroundColor: '#ffffff',
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 0 },
    shadowOpacity: 0.6,
    shadowRadius: 4,
    elevation: 2,
  },
  textContainer: {
    padding: 10,
    paddingTop: 15,
    paddingBottom: 25,
  },
  fullLine: {
    alignSelf: 'center',
    width: '80%',
    height: 1,
    marginTop: 2,
    marginBottom: 10,
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
  customButtonStyle: {
    flex: 1,
    maxWidth: 500,
    height: 40,
    minHeight: 40,
    borderRightWidth: 1,
    borderRightColor: 'white',
  },
  buttonFirst: {
    borderBottomLeftRadius: 6,
  },
  buttonLast: {
    borderBottomRightRadius: 6,
    borderRightWidth: 0,
  },
});
