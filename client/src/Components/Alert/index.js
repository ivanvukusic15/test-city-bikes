// @flow
import React, { Component } from 'react';
import { Alert } from './Alert';
import type { AlertType } from '../../Types';

export class AlertWrapper extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      alert: null,
    };
  }

  closeAlert = () => {
    this.setState({ alert: null });
  };

  render() {
    const { alert } = this.state;
    if (alert) {
      return <Alert alert={alert} close={this.closeAlert} />;
    }
    return null;
  }
}

type Props = {};
type State = {
  alert: AlertType,
};

export default AlertWrapper;
