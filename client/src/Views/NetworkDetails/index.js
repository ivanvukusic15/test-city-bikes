import React, { Component } from 'react';
import { FlatList, View } from 'react-native';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, StationItem, SearchBar, Spinner, Text } from '../../Components';
import { global } from '../../Style';
import { getNetwork, resetNetwork, updateFavouriteStations } from '../../Actions';

export class NetworkDetails extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    const count = get(navigation, 'state.params.count', 0);
    const title = get(navigation, 'state.params.network.name', 'Network details');
    return {
      headerRight: <View />,
      headerTitle: ({ style }) => {
        return (
          <View style={[global.container, global.column]}>
            <Text style={style}>{title}</Text>
            <Text style={[style, global.whiteTextColor, global.normalWeight, global.smallTextSize]}>
              {count} stations found
            </Text>
          </View>
        );
      },
    };
  };

  static getDerivedStateFromProps(props, state) {
    if (!state.sations && props.stations) {
      return {
        stations: props.stations,
      };
    }
    return null;
  }

  constructor(props) {
    super(props);
    this.state = {
      stations: [],
    };
  }

  componentWillUnmount() {
    this.props.resetNetwork();
  }

  onChangeText = searchText => {
    this.setState({ searchText });
  };

  onTryAgain = () => {
    this.props.getNetwork({
      url: get(this.props.navigation, 'state.params.network.href', null),
      errorCb: () => this.context.setGlobalError({ title: 'Something went wrong', message: 'Please try again' }),
    });
  };

  onReset = () => {
    this.setState({ searchText: '' });
  };

  onStationPress = station => () => {
    this.props.navigation.navigate('StationDetails', { station });
  };

  onStarPress = station => () => {
    this.props.updateFavouriteStations(this.props.favourites, station);
  };

  filterData = () => {
    let filteredData = this.state.stations;
    if (this.state.searchText) {
      filteredData = this.state.stations.filter(
        item => item.name && item.name.toLowerCase().includes(this.state.searchText.toLowerCase()),
      );
    }
    if (!this.props.navigation.state.params || this.props.navigation.state.params.count !== filteredData.length) {
      this.props.navigation.setParams({ count: filteredData.length });
    }
    return filteredData;
  };

  keyExtractor = item => item.id;

  renderItem = ({ item, index }) => {
    return (
      <StationItem
        favorite={!!this.props.favourites[item.id]}
        isLast={this.props.navigation.state.params && index + 1 === this.props.navigation.state.params.count}
        item={item}
        onPress={this.onStationPress(item)}
        onStarPress={this.onStarPress(item)}
      />
    );
  };

  renderData() {
    const { loading } = this.props;
    if (this.state.stations.length) {
      return (
        <FlatList
          style={global.container}
          data={this.filterData()}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
        />
      );
    }
    if (loading) {
      return <Spinner />;
    }
    return (
      <View style={[global.container, global.contentContainer, global.alignCenter, global.justifyCenter]}>
        <Text style={global.contentContainer}>No data found.</Text>
        <Button label={'Try again'} onPress={this.onTryAgain} />
      </View>
    );
  }

  render() {
    return (
      <>
        <SearchBar
          onChangeText={this.onChangeText}
          onReset={this.onReset}
          placeholder={'Filter networks by name...'}
          value={this.state.searchText}
        />
        {this.renderData()}
      </>
    );
  }
}

type Props = {};
type State = {};

const mapStateToProps = ({ network }) => {
  return {
    favourites: network.favourites,
    loading: network.loading,
    stations: network.network && network.network.stations,
  };
};

NetworkDetails.contextTypes = {
  setGlobalError: PropTypes.func,
};

export default connect(
  mapStateToProps,
  { getNetwork, resetNetwork, updateFavouriteStations },
)(NetworkDetails);
