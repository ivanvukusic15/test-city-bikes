import React, { Component } from 'react';
import { FlatList, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import { navigationOptions } from '../../Navigators/MainNavigator';
import { Button, NetworkItem, SearchBar, Spinner, Text } from '../../Components';
import { global } from '../../Style';
import { styles } from './Networks.style';
import { getNetwork, getNetworks, logout } from '../../Actions';

export class Networks extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    const count = get(navigation, 'state.params.count', 0);
    return {
      ...navigationOptions,
      title: 'Networks',
      headerLeft: <View />,
      headerTitle: ({ style, children: title }) => {
        return (
          <View style={[global.container, global.column]}>
            <Text style={style}>{title}</Text>
            <Text style={[style, global.whiteTextColor, global.normalWeight, global.smallTextSize]}>
              {count} networks found
            </Text>
          </View>
        );
      },
      headerRight: (
        <TouchableOpacity
          style={styles.logut_button}
          onPress={() => navigation.state.params && navigation.state.params.logout()}>
          <Text style={[global.smallTextSize, global.whiteTextColor]}>Log Out</Text>
        </TouchableOpacity>
      ),
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({ logout: this.logout });
    this.props.getNetworks({
      errorCb: () => this.context.setGlobalError({ title: 'Something went wrong', message: 'Please try again' }),
    });
  }

  logout = () => this.props.logout();

  onTryAgain = () => {
    this.props.getNetworks({
      errorCb: () => this.context.setGlobalError({ title: 'Something went wrong', message: 'Please try again' }),
    });
  };

  onChangeText = searchText => {
    this.setState({ searchText });
  };

  onReset = () => {
    this.setState({ searchText: '' });
  };

  onNetworkPress = network => () => {
    this.props.getNetwork({
      url: network.href,
      errorCb: () => this.context.setGlobalError({ title: 'Something went wrong', message: 'Please try again' }),
    });
    this.props.navigation.navigate('NetworkDetails', { network });
  };

  filterData = data => {
    let filteredData = data;
    if (this.state.searchText) {
      filteredData = data.filter(
        item => item.name && item.name.toLowerCase().includes(this.state.searchText.toLowerCase()),
      );
    }
    if (!this.props.navigation.state.params || this.props.navigation.state.params.count !== filteredData.length) {
      this.props.navigation.setParams({ count: filteredData.length });
    }
    return filteredData;
  };

  keyExtractor = item => item.id;

  renderItem = ({ item, index }) => {
    return (
      <NetworkItem
        color={item.location && this.props.countries[item.location.country]}
        country={item.location && item.location.country}
        city={item.location && item.location.city}
        isLast={this.props.navigation.state.params && index + 1 === this.props.navigation.state.params.count}
        name={item.name}
        onPress={this.onNetworkPress(item)}
      />
    );
  };

  renderData() {
    const { loading, networks } = this.props;
    if (networks && networks.length) {
      return (
        <FlatList data={this.filterData(networks)} keyExtractor={this.keyExtractor} renderItem={this.renderItem} />
      );
    }
    if (loading) {
      return <Spinner />;
    }
    return (
      <View style={[global.container, global.contentContainer, global.alignCenter, global.justifyCenter]}>
        <Text style={global.contentContainer}>No data found.</Text>
        <Button label={'Try again'} onPress={this.onTryAgain} />
      </View>
    );
  }

  render() {
    return (
      <>
        <SearchBar
          onChangeText={this.onChangeText}
          onReset={this.onReset}
          placeholder={'Filter networks by name...'}
          value={this.state.searchText}
        />
        {this.renderData()}
      </>
    );
  }
}

type Props = {};
type State = {};

const mapStateToProps = ({ network }) => {
  return {
    countries: network.countries,
    loading: network.loading,
    networks: network.networks,
  };
};

Networks.contextTypes = {
  setGlobalError: PropTypes.func,
};

export default connect(
  mapStateToProps,
  { getNetwork, getNetworks, logout },
)(Networks);
