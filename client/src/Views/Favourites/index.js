import React, { Component } from 'react';
import { FlatList, View } from 'react-native';
import { get, toArray } from 'lodash';
import { connect } from 'react-redux';
import { navigationOptions } from '../../Navigators/MainNavigator';
import { SearchBar, Spinner, StationItem, Text } from '../../Components';
import { global } from '../../Style';
import { updateFavouriteStations } from '../../Actions';

export class Favourites extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    const count = get(navigation, 'state.params.count', 0);
    return {
      ...navigationOptions,
      title: 'Networks',
      headerTitle: ({ style, children: title }) => {
        return (
          <View style={[global.container, global.column]}>
            <Text style={style}>{title}</Text>
            <Text style={[style, global.whiteTextColor, global.normalWeight, global.smallTextSize]}>
              {count} networks found
            </Text>
          </View>
        );
      },
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
    };
  }

  onChangeText = searchText => {
    this.setState({ searchText });
  };

  onReset = () => {
    this.setState({ searchText: '' });
  };

  onStationPress = station => () => {
    this.props.navigation.navigate('StationDetails', { station });
  };

  onStarPress = station => () => {
    this.props.updateFavouriteStations(this.props.favourites, station);
  };

  filterData = () => {
    let filteredData = toArray(this.props.favourites);
    if (this.state.searchText) {
      filteredData = filteredData.filter(
        item => item.name && item.name.toLowerCase().includes(this.state.searchText.toLowerCase()),
      );
    }
    if (
      !this.props.navigation.state.params ||
      (this.props.navigation.state.params &&
        this.props.navigation.state.params.count &&
        this.props.navigation.state.params.count !== filteredData.length)
    ) {
      this.props.navigation.setParams({ count: filteredData.length });
    }
    return filteredData;
  };

  keyExtractor = item => item.id;

  renderItem = ({ item, index }) => {
    return (
      <StationItem
        favorite
        isLast={this.props.navigation.state.params && index + 1 === this.props.navigation.state.params.count}
        item={item}
        onPress={this.onStationPress(item)}
        onStarPress={this.onStarPress(item)}
      />
    );
  };

  renderData() {
    const { loading } = this.props;
    if (this.props.favourites && Object.keys(this.props.favourites).length) {
      return (
        <FlatList
          style={global.container}
          data={this.filterData(this.props.favourites)}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
        />
      );
    }
    if (loading) {
      return <Spinner />;
    }
    return (
      <View style={[global.container, global.contentContainer, global.alignCenter, global.justifyCenter]}>
        <Text style={global.contentContainer}>No data found.</Text>
      </View>
    );
  }

  render() {
    return (
      <>
        <SearchBar
          onChangeText={this.onChangeText}
          onReset={this.onReset}
          placeholder={'Filter networks by name...'}
          value={this.state.searchText}
        />
        {this.renderData()}
      </>
    );
  }
}

type Props = {};
type State = {};

const mapStateToProps = ({ network }) => {
  return {
    favourites: network.favourites && network.favourites,
  };
};

export default connect(
  mapStateToProps,
  { updateFavouriteStations },
)(Favourites);
