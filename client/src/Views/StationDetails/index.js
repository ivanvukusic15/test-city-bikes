import React, { Component } from 'react';
import { View } from 'react-native';
// Uncomment to use google maps
// import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import MapView, { Marker } from 'react-native-maps';
import get from 'lodash/get';
import { Text } from '../../Components';
import { global } from '../../Style';

export class StationDetails extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    const freeBikes = get(navigation, 'state.params.station.free_bikes', 0);
    return {
      title: 'NetworkDetails',
      headerRight: <View />,
      headerTitle: ({ style, children: title }) => {
        return (
          <View style={[global.container, global.column]}>
            <Text style={style}>{title}</Text>
            <Text style={[style, global.whiteTextColor, global.normalWeight, global.smallTextSize]}>
              {freeBikes} free bikes
            </Text>
          </View>
        );
      },
    };
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const station = get(this.props.navigation.state, 'params.station', null);
    return (
      <View style={global.container}>
        <MapView
          style={global.absoluteFull}
          // Uncomment to use google maps
          // provider={PROVIDER_GOOGLE}
          initialRegion={{
            latitude: station.latitude,
            longitude: station.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}>
          {station && <Marker coordinate={{ latitude: station.latitude, longitude: station.longitude }} />}
        </MapView>
      </View>
    );
  }
}

type Props = {};
type State = {};

export default StationDetails;
