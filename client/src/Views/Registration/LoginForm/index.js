import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Button, Text, TextInput } from '../../../Components';
import { styles } from './LoginForm.style';
import { global, themeRed } from '../../../Style';
import { checkValidity } from '../../../Utils';
import { login } from '../../../Actions';

export class LoginForm extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      email: {
        error: '',
        keyboardType: 'email-address',
        label: 'Email:',
        value: '',
        rules: {
          required: true,
          isEmail: true,
        },
        touched: false,
        isValid: false,
      },
      password: {
        error: '',
        label: 'Password:',
        value: '',
        rules: {
          required: true,
        },
        secureTextEntry: true,
        touched: false,
        isValid: false,
      },
      error: '',
      loading: false,
    };
  }

  onChangeText = field => value => {
    if (field === 'email') {
      value = value.toLowerCase();
    }
    this.setState(state => {
      let validity = { valid: state.valid, error: state.error };
      if (state[field].touched) {
        validity = checkValidity(field, value, state);
      }
      return {
        [field]: {
          ...state[field],
          error: validity.error,
          value,
          isValid: validity.isValid,
          loading: false,
          touched: true,
        },
      };
    });
  };

  onLoginPress = () => {
    this.setState({ loading: true }, () => {
      this.props.login({
        email: this.state.email.value,
        password: this.state.password.value,
        cb: () => {
          this.setState({ loading: false });
          this.props.goToDashboard();
        },
        errorCb: message => {
          this.setState({ loading: false });
          this.context.setGlobalError({ title: 'Login error', message });
        },
      });
    });
  };

  render() {
    const { email, error, loading, password } = this.state;
    let disabled = false;
    if (!email.value || !email.isValid || !password.value || !password.isValid) {
      disabled = true;
    }
    return (
      <View style={[global.contentContainer, styles.containerWidth]}>
        <View style={[global.container, global.column, global.justifyStart]}>
          <TextInput
            error={email.error}
            iconName={!!email.error && 'ban'}
            iconColor={!!email.error && themeRed}
            iconSize={20}
            label={email.label}
            keyboardType={email.keyboardType}
            value={email.value}
            onChangeText={this.onChangeText('email')}
          />
          <TextInput
            error={password.error}
            iconName={!!password.error && 'ban'}
            iconColor={!!password.error && themeRed}
            iconSize={20}
            keyboardType={password.keyboardType}
            label={password.label}
            secureTextEntry={password.secureTextEntry}
            value={password.value}
            onChangeText={this.onChangeText('password')}
          />
          <Text style={[global.center, global.redTextColor]}>{error}</Text>
        </View>
        <Button
          customStyle={disabled && global.buttonDisabled}
          disabled={disabled}
          label="Login"
          loading={loading}
          onPress={this.onLoginPress}
        />
      </View>
    );
  }
}

type Props = {
  goToDashboard: Function,
  login: Function,
};
type State = {};

LoginForm.contextTypes = {
  setGlobalError: PropTypes.func,
};

export default connect(
  null,
  { login },
)(LoginForm);
