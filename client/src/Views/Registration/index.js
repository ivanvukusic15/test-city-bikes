import React, { Component } from 'react';
import { View } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { TabsScroll } from '../../Components';
import LoginForm from './LoginForm';
import RegistrationForm from './RegistrationForm';
import { global } from '../../Style';

export class Registration extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  goToDashboard = () => {
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Dashboard' })],
      }),
    );
  };

  render() {
    return (
      <View style={global.container}>
        <TabsScroll tabs={['login', 'registration']}>
          <LoginForm goToDashboard={this.goToDashboard} />
          <RegistrationForm goToDashboard={this.goToDashboard} />
        </TabsScroll>
      </View>
    );
  }
}

type Props = {};
type State = {};

export default Registration;
