import { Dimensions, StyleSheet } from 'react-native';
import { bottomDiff } from '../../../Style/index';

const { width } = Dimensions.get('screen');

export const styles = StyleSheet.create({
  containerWidth: {
    width,
    paddingBottom: 15 + bottomDiff,
  },
});
