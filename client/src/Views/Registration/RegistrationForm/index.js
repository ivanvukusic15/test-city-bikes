import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Button, Text, TextInput } from '../../../Components';
import { styles } from './RegistrationForm.style';
import { global, green, themeRed } from '../../../Style';
import { checkValidity } from '../../../Utils';
import { emailVerify, register } from '../../../Actions';

export class RegistrationForm extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      email: {
        error: '',
        keyboardType: 'email-address',
        label: 'Email:',
        value: '',
        rules: {
          required: true,
          isEmail: true,
        },
        touched: false,
        isValid: false,
      },
      password: {
        error: '',
        label: 'Password:',
        value: '',
        rules: {
          required: true,
          minLength: 6,
        },
        secureTextEntry: true,
        touched: false,
        isValid: false,
      },
      confirmPassword: {
        error: '',
        label: 'Repeat password:',
        value: '',
        rules: {
          required: true,
          minLength: 6,
          sameAs: 'password',
        },
        secureTextEntry: true,
        touched: false,
        isValid: false,
      },
      error: '',
      loading: false,
    };
  }

  onChangeText = field => async value => {
    let isValid = false;
    await this.setState(state => {
      let validity = { valid: state.valid, error: state.error };
      if (state[field].touched) {
        validity = checkValidity(field, value, state);
        /* eslint-disable-next-line */
        isValid = validity.isValid;
      }
      return {
        [field]: {
          ...state[field],
          error: validity.error,
          value,
          isValid,
          loading: false,
          touched: true,
        },
      };
    });
    if (field === 'email' && isValid) {
      emailVerify({
        email: value.toLowerCase(),
        cb: code => {
          if (code && value === this.state.email.value) {
            this.setState(state => {
              return {
                email: {
                  ...state.email,
                  loading: false,
                  isValid: false,
                  error: 'User already exists with that email address.',
                },
              };
            });
          } else {
            this.setState(state => {
              return {
                email: {
                  ...state.email,
                  loading: false,
                },
              };
            });
          }
        },
      });
    }
  };

  onRegisterPress = () => {
    this.setState({ loading: true }, () => {
      this.props.register({
        confirmPassword: this.state.confirmPassword.value,
        email: this.state.email.value,
        password: this.state.password.value,
        cb: () => {
          this.setState({ laoding: false });
          this.props.goToDashboard();
        },
        errorCb: message => {
          this.setState({ laoding: false });
          this.context.setGlobalError({ title: 'Registration error', message });
        },
      });
    });
  };

  render() {
    const { confirmPassword, email, error, loading, password } = this.state;
    let disabled = false;
    if (
      !email.value ||
      !email.isValid ||
      !password.value ||
      !password.isValid ||
      !confirmPassword.value ||
      !confirmPassword.isValid
    ) {
      disabled = true;
    }
    return (
      <View style={[global.contentContainer, styles.containerWidth]}>
        <View style={[global.container, global.column, global.justifyStart]}>
          <TextInput
            error={email.error}
            iconName={(!!email.error && 'ban') || (email.isValid && 'check')}
            iconColor={(!!email.error && themeRed) || (email.isValid && green)}
            iconSize={20}
            label={email.label}
            keyboardType={email.keyboardType}
            secureTextEntry={email.secureTextEntry}
            loading={email.loading}
            value={email.value}
            onChangeText={this.onChangeText('email')}
          />
          <TextInput
            error={password.error}
            iconName={(!!password.error && 'ban') || (password.isValid && 'check')}
            iconColor={(!!password.error && themeRed) || (password.isValid && green)}
            iconSize={20}
            keyboardType={password.keyboardType}
            secureTextEntry={password.secureTextEntry}
            label={password.label}
            value={password.value}
            onChangeText={this.onChangeText('password')}
          />
          <TextInput
            error={confirmPassword.error}
            iconName={(!!confirmPassword.error && 'ban') || (confirmPassword.isValid && 'check')}
            iconColor={(!!confirmPassword.error && themeRed) || (confirmPassword.isValid && green)}
            iconSize={20}
            keyboardType={confirmPassword.keyboardType}
            secureTextEntry={confirmPassword.secureTextEntry}
            label={confirmPassword.label}
            value={confirmPassword.value}
            onChangeText={this.onChangeText('confirmPassword')}
          />
          <Text style={[global.center, global.redTextColor]} numberOfLines={1}>
            {error}
          </Text>
        </View>
        <Button
          customStyle={disabled && global.buttonDisabled}
          disabled={disabled}
          label="Register"
          loading={loading}
          onPress={this.onRegisterPress}
        />
      </View>
    );
  }
}

type Props = {
  goToDashboard: Function,
  register: Function,
};
type State = {};

RegistrationForm.contextTypes = {
  setGlobalError: PropTypes.func,
};

export default connect(
  null,
  { register },
)(RegistrationForm);
