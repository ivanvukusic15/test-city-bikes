import { AppNavigator } from '../Navigators/MainNavigator';
import { INITIAL_DATA, TYPE_USER_LOGOUT } from '../Constants';

const INITIAL_STATE = AppNavigator.router.getStateForAction(
  AppNavigator.router.getActionForPathAndParams('Registration'),
);

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case INITIAL_DATA:
      return { ...action.payload.nav };
    case TYPE_USER_LOGOUT:
      return { ...INITIAL_STATE };
    default:
      const nextState = AppNavigator.router.getStateForAction(action, state); // eslint-disable-line
      return nextState || state;
  }
};
