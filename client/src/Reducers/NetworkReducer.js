import {
  INITIAL_DATA,
  TYPE_USER_LOGOUT,
  TYPE_NETWORKS_GET,
  TYPE_NETWORK_GET,
  TYPE_NETWORK_RESET,
  TYPE_NETWORKS_UPDATE,
  TYPE_NETWORK_UPDATE_FAVOURITES,
} from '../Constants';

const INITIAL_STATE = {
  countries: null,
  favourites: {},
  loading: false,
  networks: null,
  network: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case INITIAL_DATA:
      return { ...state, favourites: { ...action.payload.favourites } };
    case TYPE_NETWORKS_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value };
    case TYPE_NETWORKS_GET:
      return { ...state, countries: action.payload.countries, networks: action.payload.networks, loading: false };
    case TYPE_NETWORK_GET:
      return { ...state, network: action.payload, loading: false };
    case TYPE_NETWORK_RESET:
      return { ...state, network: null };
    case TYPE_NETWORK_UPDATE_FAVOURITES:
      return { ...state, favourites: { ...action.payload.favourites }, loading: false };
    case TYPE_USER_LOGOUT:
      return { ...INITIAL_STATE };
    default:
      return state;
  }
};
