import { TYPE_HELPER_UPDATE, TYPE_USER_LOGOUT } from '../Constants';

const INITIAL_STATE = {
  error: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPE_HELPER_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value };
    case TYPE_USER_LOGOUT:
      return { ...INITIAL_STATE };
    default:
      return state;
  }
};
