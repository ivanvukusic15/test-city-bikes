import { combineReducers } from 'redux';
import helper from './HelperReducer';
import nav from './NavReducer';
import network from './NetworkReducer';
import user from './UserReducer';

export default combineReducers({
  helper,
  nav,
  network,
  user,
});
