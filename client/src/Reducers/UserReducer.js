import { TYPE_USER_UPDATE, TYPE_USER_LOGIN, TYPE_USER_LOGOUT } from '../Constants';

const INITIAL_STATE = {
  accessToken: false,
  error: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPE_USER_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value };
    case TYPE_USER_LOGIN:
      return { ...state, accessToken: action.payload.accessToken };
    case TYPE_USER_LOGOUT:
      return { ...INITIAL_STATE };
    default:
      return state;
  }
};
