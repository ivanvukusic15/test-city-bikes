import { Dimensions, Platform } from 'react-native';
import moment from 'moment';

const { width, height } = Dimensions.get('window');

export const camelCase = str => {
  str.replace(/^\s+|\s+$|\s+(?=\s)/g, '').toLowerCase();
  const array = str.split(' ');
  let value = '';
  array.map((s, i) => {
    if (s[0]) {
      value += s[0].toUpperCase() + s.slice(1);
      if (i + 1 < array.length) {
        value += ' ';
      }
    }
  });
  return value;
};

export const firstToUpperCase = str => str && str[0].toUpperCase() + str.slice(1);

export const addressString = address => {
  let addressString = '';
  if (address.address) {
    addressString += address.address;
  }
  if (address.address && address.city) {
    if (addressString.length > 0) {
      addressString += ', ';
    }
    addressString += address.city;
  }
  if (address.address && address.state && address.state.label) {
    if (address.address && address.city) {
      addressString += ', ';
    }
    addressString += address.state.label;
  }
  return addressString;
};

export const centsToDollars = amount => {
  if (typeof amount !== 'string' && typeof amount !== 'number') {
    throw new Error('Amount passed must be of type String or Number.');
  }

  return (parseFloat(amount) / 100).toFixed(2);
};

export const birthdayMask = (input, birthday) => {
  let inputValue = input;
  if (inputValue.length < birthday.length) {
    return inputValue;
  }
  /**
   * MONTH
   * */
  if (inputValue.length === 2 && /00/.test(inputValue)) {
    inputValue = '01';
  }
  if (inputValue.length === 2 && (/\s/.test(inputValue[1]) || /\s/.test(inputValue[0]))) {
    inputValue = '01';
  }
  if (inputValue.length === 2 && (/1/.test(inputValue[0]) && /[3-9]/.test(inputValue[1]))) {
    inputValue = '01';
  }
  if (inputValue.length === 1 && /[2-9]/.test(inputValue)) {
    inputValue = `0${inputValue}`;
  }
  if (inputValue.length === 2) {
    inputValue += '/';
  }
  /**
   * DAY
   * */
  // Changing invalid day to valid days 4* to 04
  if (inputValue.length === 4 && /[4-9]/.test(inputValue[3])) {
    inputValue = `${inputValue.slice(0, inputValue.length - 1)}0${inputValue[3]}`;
  }
  // If placeholders empty
  if ((inputValue.length === 4 && /\s/.test(inputValue[3])) || (inputValue.length === 5 && /\s/.test(inputValue[4]))) {
    inputValue = inputValue.slice(0, inputValue.length - 1);
  }
  // Change to 01 if 00
  if (inputValue.length === 5 && /0/.test(inputValue[3]) && /0/.test(inputValue[4])) {
    inputValue = `${inputValue.slice(0, inputValue.length - 1)}1`;
  }
  // Februar with 28 days
  if (inputValue.length === 4 && (/0/.test(inputValue[0]) && /2/.test(inputValue[1])) && /3/.test(inputValue[3])) {
    inputValue = `${inputValue.slice(0, inputValue.length - 1)}28`;
  }

  // Months with 30 days
  if (
    inputValue.length === 5 &&
    ((/1/.test(inputValue[0]) && /1/.test(inputValue[1])) || /4|6|9/.test(inputValue[1])) &&
    /3/.test(inputValue[3]) &&
    /[1-9]/.test(inputValue[4])
  ) {
    inputValue = `${inputValue.slice(0, inputValue.length - 1)}0`;
  }
  // Months with 31 days
  if (
    inputValue.length === 5 &&
    ((/1/.test(inputValue[0]) && /2/.test(inputValue[1])) || /1|3|5|0/.test(inputValue[1])) &&
    /3/.test(inputValue[3]) &&
    /[2-9]/.test(inputValue[4])
  ) {
    inputValue = `${inputValue.slice(0, inputValue.length - 1)}1`;
  }
  // Slash after month
  if (inputValue.length === 5) {
    inputValue += '/';
  }

  /**
   * YEAR
   * */
  if (inputValue.length === 7 && (/1|[3-9]/.test(inputValue[6]) || /0/.test(inputValue[6]))) {
    inputValue = `${inputValue.slice(0, inputValue.length - 1)}19`;
  }
  if (inputValue.length === 7 && /2/.test(inputValue[6])) {
    inputValue = `${inputValue.slice(0, inputValue.length - 1) + inputValue[6]}0`;
  }
  if (inputValue.length === 11 && /./.test(inputValue[10])) {
    inputValue = inputValue.slice(0, inputValue.length - 1);
  }
  return inputValue;
};

export const creditCardDayMask = (input, birthday) => {
  let inputValue = input;
  if (inputValue.length < birthday.length) {
    return inputValue;
  }
  /**
   * MONTH
   * */
  if (inputValue.length === 2 && /00/.test(inputValue)) {
    inputValue = '01';
  }
  if (inputValue.length === 2 && (/\s/.test(inputValue[1]) || /\s/.test(inputValue[0]))) {
    inputValue = '01';
  }
  if (inputValue.length === 2 && (/1/.test(inputValue[0]) && /[3-9]/.test(inputValue[1]))) {
    inputValue = '01';
  }
  if (inputValue.length === 1 && /[2-9]/.test(inputValue)) {
    inputValue = `0${inputValue}`;
  }
  if (inputValue.length === 2) {
    inputValue += '/';
  }
  return inputValue;
};

export const onlyNumbers = input => {
  return input.replace(/[^0-9]/g, '');
};

export const iPhoneX = () => {
  return (
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    (height === 812 || width === 812 || (height === 896 || width === 896))
  );
};

export const isTablet = () => {
  return width >= 600;
};

export const checkValidity = (field: string, value: string, object: Object) => {
  const { rules } = object[field];
  let isValid = true;
  let error = '';
  if (!rules) {
    return true;
  }
  const newField = firstToUpperCase(field.replace(/([a-z](?=[A-Z]))/g, '$1 ').toLowerCase());
  if (rules.required && rules.type === 'checkbox') {
    isValid = value && isValid;
    if (!value) {
      error = `This field must be checked. `;
    }
  } else if (rules.required) {
    if (value && (typeof value === 'string' || value instanceof String)) {
      // eslint-disable-next-line
      value = value.trim();
    }
    isValid = !!value && isValid;
    if (!value) {
      error = `${newField} cannot be empty. `;
    }
  }
  if (rules.minLength) {
    isValid = value.length >= rules.minLength && isValid;

    if (value.length < rules.minLength) {
      error += `Must have at least ${rules.minLength} characters. `;
    }
  }
  if (rules.maxLength) {
    isValid = value.length <= rules.maxLength && isValid;
    if (value.length > rules.maxLength) {
      error += `${newField} cannot have more than ${rules.maxLength} characters. `;
    }
  }
  if (rules.sameAs) {
    isValid = value && value === object[rules.sameAs].value && isValid;
    if (value && value !== object[rules.sameAs].value) {
      error = `${newField} not same as ${rules.sameAs}`;
    }
  }
  if (rules.isEmail) {
    const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    isValid = pattern.test(value) && isValid;
    if (!pattern.test(value)) {
      error += `Email not valid. `;
    }
  }
  if (rules.isNumeric) {
    const pattern = /^\d+$/;
    isValid = pattern.test(value) && isValid;
    if (!pattern.test(value)) {
      error += `${newField} must be a number. `;
    }
  }
  if (rules.cardDate) {
    let format = value.length === 5 ? 'MM/YY' : 'MM/YYYY';
    const numberOfDays = moment(value, format).daysInMonth();
    format = value.length === 5 ? 'DD/MM/YY' : 'DD/MM/YYYY';
    isValid =
      moment(`${numberOfDays}/${value}`, format).isValid() &&
      moment(`${numberOfDays}/${value}`, format).isAfter(moment()) &&
      isValid;
    if (value.length > 4 && moment(`${numberOfDays}/${value}`, format).isAfter(rules.cardDate)) {
      error += `${newField} is not a valid date. `;
    }
    if (value.length > 4 && moment(`${numberOfDays}/${value}`, format).isBefore(moment())) {
      error += `${newField} cannot be in the past. `;
    }
  }

  if (rules.child && Object.keys(rules.child).length && object && Object.keys(object).length) {
    let childErrors = '';
    let childIsValid = true;
    Object.keys(rules.child).map(key => {
      const valid = checkValidity(key, object[key].value, rules.child[key]);
      childIsValid = valid.isValid && childIsValid;
      childErrors += valid.error;
    });
    if (field === 'fullAddress' && error.length > 0) {
      error = `Please click on the button on the right to fill all the data.${childErrors}`;
    }
    isValid = childIsValid && isValid;
  }
  return { error, isValid };
};

export const validateForm = (fields: Array<string>, values: Object) => {
  const newState = {};
  let isValid = true;
  if (fields && fields.length) {
    if (values && Object.keys(values).length) {
      fields.map(field => {
        if (values[field]) {
          const validity = checkValidity(field, values[field].value, values[field].rules, values);
          if (!validity.isValid) {
            isValid = false;
          }
          newState[field] = {
            ...values[field],
            isValid: validity.isValid,
            error: validity.error,
            touched: true,
          };
        }
      });
    }
  }
  return { isValid, newState };
};
