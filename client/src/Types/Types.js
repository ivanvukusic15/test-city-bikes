type AlertButtonType = {
  action: Function,
  label: string,
};

type AlertType = {
  buttons: Array<AlertButtonType>,
  message: string,
  title: string,
};

type DoneBarType = {
  current: Function,
  inventoryItemRefs: Object,
  scrollView: any,
};

type ErrorType = {
  message: string,
  title: string,
};

type FormValidationType = {
  error: string,
  icon: string,
  keyboardType: string,
  placeholder: string,
  rules: ValidationRulesType,
  secureTextEntry: boolean,
  touched: boolean,
  isValid: boolean,
  value: string,
};

type ValidationRulesType = {
  required: boolean,
  minLength: number,
  maxLenght: number,
  isEmail: Boolean,
  isSame: string,
  isNumberic: Boolean,
};

export { AlertType, DoneBarType, ErrorType, FormValidationType, ValidationRulesType };
