/**
 * @flow
 */
import { createStackNavigator } from 'react-navigation';
import { global, headerRegistrationColor } from '../Style';

import Registration from '../Views/Registration';
import TabNavigator from './TabNavigator';
import NetworkDetails from '../Views/NetworkDetails';
import StationDetails from '../Views/StationDetails';

export const navigationOptions = {
  headerStyle: global.headerStyle,
  headerTitleStyle: [
    global.fontFamily,
    global.center,
    global.boldWeight,
    global.whiteTextColor,
    global.h5,
    { alignSelf: 'center' },
  ],
  headerTintColor: 'white',
  headerBackTitle: null,
};
const navigationOptionsRegistration = {
  ...navigationOptions,
  headerStyle: { ...global.headerStyle, backgroundColor: headerRegistrationColor },
};

export const AppNavigator = createStackNavigator({
  Registration: {
    screen: Registration,
    navigationOptions: {
      ...navigationOptionsRegistration,
      title: 'Registration',
    },
  },
  Dashboard: {
    screen: TabNavigator,
    navigationOptions: {
      ...navigationOptions,
      header: null,
    },
  },
  NetworkDetails: {
    screen: NetworkDetails,
    navigationOptions: {
      ...navigationOptions,
    },
  },
  StationDetails: {
    screen: StationDetails,
    navigationOptions: {
      ...navigationOptions,
    },
  },
});
