import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import { headerColor, largerTextSize } from '../Style';

import Networks from '../Views/Networks';
import Favourites from '../Views/Favourites';

const tabBarOptions = {
  style: {
    backgroundColor: headerColor,
  },
  labelStyle: {
    fontSize: largerTextSize,
  },
  activeTintColor: '#FFFFFF',
  inactiveTintColor: '#FAFAFABF',
  showIcon: false,
};

const TabNavigator = createBottomTabNavigator(
  {
    Networks: {
      screen: createStackNavigator({ Networks }),
      navigationOptions: () => ({
        tabBarLabel: 'Networks',
        titleStyle: {
          color: '#FFFFFF',
        },
      }),
    },
    Favourites: {
      screen: createStackNavigator({ Favourites }),
      navigationOptions: () => ({
        tabBarLabel: 'Favourites',
      }),
    },
  },
  {
    initialRouteName: 'Networks',
    tabBarOptions,
    tabBarPosition: 'bottom',
  },
);

export default TabNavigator;
