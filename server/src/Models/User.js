import { Schema, model } from 'mongoose';
import bcrypt from 'bcrypt';

export const UserSchema = new Schema({
  createdAt: { type: Date, default: Date.now() },
  updatedAt: { type: Date, default: Date.now() },
  email: {
    type: String,
    default: '',
    unique: true,
    lowercase: true,
  },
  username: {
    type: String,
    default: '',
    unique: true,
  },
  password: {
    type: String,
    default: '',
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
});

const createUser = (newUser, callback) => {
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(newUser.password, salt, function(_, hash) {
      newUser.password = hash;
      newUser.save(callback);
    });
  });
};
UserSchema.methods.createUser = createUser;

const comparePassword = (candidatePassword, hash, callback) => {
  bcrypt.compare(candidatePassword, hash, callback);
};
UserSchema.methods.comparePassword = comparePassword;

export const User = model('User', UserSchema);
