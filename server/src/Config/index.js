import ip from 'ip';
import dotenv from 'dotenv-safe';

dotenv.load();

const dbUrl = process.env.DB_URL;
const dbUsername = process.env.DB_USERNAME;
const dbPassword = process.env.DB_PASSWORD;

export default {
  ip: ip.address(),
  port: process.env.PORT || 3000,
  dbUrl: `mongodb://${dbUsername}:${dbPassword}@${dbUrl}`,
  jwtSecret: 'jwtSecret',
};
