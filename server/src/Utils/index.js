import jwt from 'jsonwebtoken';
import config from '../Config';

export const createToken = (req, res, userData) =>
  jwt.sign(userData, config.jwtSecret, { expiresIn: 60 * 60 * 24 * 365 * 10 });
