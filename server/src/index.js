import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import chalk from 'chalk';
import config from './Config';

const app = express();

const indexRoutes = require('./Routes/index');

mongoose.connect(config.dbUrl, { useNewUrlParser: true }).then(
  () => {
    console.log('Successfuly connected to DB');
  },
  err => console.log('Error connecting to DB: ', err),
);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(
  require('express-session')({
    secret: 'Authentication via node.js',
    resave: false,
    saveUninitialized: false,
  }),
);

app.use(indexRoutes);

app.listen(config.port, config.ip, () => {
  console.log(chalk.yellow(`Server is listening on ${config.ip}:${config.port}`));
});
