import express from 'express';
import { User } from '../Models/User';
import { createToken } from '../Utils/index';

const router = express.Router();

router.post('/auth/register', (req, res) => {
  const { email, password, confirmPassword } = req.body;
  if (!email || !password || !confirmPassword) {
    return res.status(200).json({
      error: {
        code: 'missingVariables',
        message: 'Missing variables',
      },
    });
  }
  if (password !== confirmPassword) {
    return res.status(200).json({
      error: {
        code: 'paswordMissmatch',
        message: 'Password does not match confirm passowrd',
      },
    });
  }
  const newUser = new User({ email: email.toLowerCase(), username: email, password });
  return newUser.createUser(newUser, (error, user) => {
    if (error) {
      if (error.code) {
        return res.status(res.statusCode).json({
          error: {
            code: 'emailExists',
            message: 'User already exists',
          },
        });
      }
      return res.status(res.statusCode).json({
        error: {
          code: 'serverError',
          message: 'Server error',
        },
      });
    }
    const { _id: id, createdAt } = user;
    const userData = { id, email, createdAt };
    const accessToken = createToken(req, res, userData);
    return res.status(res.statusCode).json({
      success: true,
      data: {
        user: userData,
        accessToken,
      },
    });
  });
});

router.post('/auth/login', (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    return res.status(200).json({
      error: {
        code: 'missingVariables',
        message: 'Missing variables',
      },
    });
  }
  return User.findOne({ email: email.toLowerCase() }, (err, user) => {
    if (err) {
      return res.status(res.statusCode).json({
        error: {
          code: 'serverError',
          message: 'Server error',
        },
      });
    }
    if (user) {
      return user.comparePassword(password, user.password, (error, isMatch) => {
        if (error) {
          return res.status(res.statusCode).json({
            error: {
              code: 'wrongUsernamePassword',
              message: 'Wrong email or password',
            },
          });
        }
        if (isMatch) {
          const { _id: id, createdAt } = user;
          const userData = { id, email, createdAt };
          const accessToken = createToken(req, res, userData);
          return res.status(res.statusCode).json({
            data: {
              user: userData,
              accessToken,
            },
          });
        }
        return res.status(res.statusCode).json({
          error: {
            code: 'wrongUsernamePassword',
            message: 'Wrong email or password',
          },
        });
      });
    }
    return res.status(res.statusCode).json({
      error: {
        code: 'existsNot',
        message: 'User does not exist',
      },
    });
  });
});

router.post('/auth/emailVerify', (req, res) => {
  const { email } = req.body;
  if (!email) {
    return res.status(200).json({
      error: {
        code: 'missingVariables',
        message: 'Missing variables',
      },
    });
  }
  return User.findOne({ email }, (err, user) => {
    if (err) {
      return res.status(res.statusCode).json({
        error: {
          code: 'error',
          message: 'Error',
        },
      });
    }
    if (user) {
      return res.status(200).json({
        data: {
          code: 'exists',
          message: 'Email exists',
        },
      });
    }
    return res.status(res.statusCode).json({
      data: {
        code: 'emailNotExists',
        message: 'Email does not exist',
      },
    });
  });
});

module.exports = router;
