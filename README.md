# test-city-bikes

## Project structure
```
├── server           <==== server side - node.js application
│   ├── src
│   │  ├── Config
│   │  ├── Models
│   │  ├── Routes
│   │  ├── Utils
│   │  └──  index.js
│   ├── .babelrc
│   ├── .eslintrc
│   ├── .gitignore
│   ├── .prettierrc
│   ├── package-lock.json
│   └──  package.json
├── client           <==== client side - react native application
│   ├── android
│   ├── ios
│   ├── jest
│   ├── src
│   │  ├── Actions
│   │  ├── Components
│   │  ├── Constants
│   │  ├── Navigators
│   │  ├── Reducers
│   │  ├── Style
│   │  ├── Types
│   │  ├── Utils
│   │  ├── Views
│   │  ├── AppWithNavigationState.js
│   │  └──  index.js
│   ├── .buckconfig
│   ├── .eslintrc
│   ├── .flowconfig
│   ├── .gitattributes
│   ├── .gitignore
│   ├── .prettierrc
│   ├── .watchmanconfig
│   ├── app.json
│   ├── babel.config.js
│   ├── index.js
│   ├── metro.config.js
│   ├── package-lock.json
│   ├── package.json
└── └──  yarn-lock.json
```

## Installation
##  Server side

Navigate to server folder and run the following command:

required NODE MODULES
```
npm install
```
start server
```
npm start
```

When starting you'll get starting ip address and port of your server. Use that path as your LOCAL_URL variable.
Variable path: ./client/src/Constants/index.js



##  Client side
Navigate to client folder and run the following command:

Installing required node_modules
```
npm install
```

Installing required pods
```
cd ./client/ios && pod install
```

Start iOS application
```
react-native run-ios
```

Start android application
```
react-native run-android
```

##  Usign google maps on iOS

For use google maps on iOS uncomment lines marked with // Uncomment to use google maps.

Files to be changes:
./client/ios/Podfile
./client/ios/testCityBikes/AppDelegate.m
./client/src/Views/StationDetails/index.js

After that, go to ./client/src/ios and run: 
```
pod install
```